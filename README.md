# nginx proxy on openshift

### This is Based on OpenShift example projects: 
- [nginx-ex](https://github.com/sclorg/nginx-ex) for README and OpenShift template
- [nginx-container](https://github.com/sclorg/nginx-container) for example project
- example of [nginx configurations](https://github.com/sclorg/nginx-container/tree/master/examples/1.16/test-app) 

## Instructions 
### Create website (only the first time)
- [Create CERN web site](https://webservices.web.cern.ch/webservices/Services/ManageSite/)
- [Manage accstats web site](https://webservices.web.cern.ch/webservices/Services/ManageSite/default.aspx?SiteName=accstats)
- [Make it visible on the internet](https://webservices.web.cern.ch/webservices/Tools/Permissions/default.aspx?SiteName=accstats)

### Build and Deploy on OpenShift
Build and deploy the project [accstats-nginx.git](https://gitlab.cern.ch/acclog/accsats-nginx.git)
```
oc login https://openshift.cern.ch      # openshift-dev for test web sites
oc project [project-name], e.g. acc-stats
oc new-app centos/nginx-116-centos7:latest~https://gitlab.cern.ch/acclog/accstats-nginx.git
oc expose svc/accstats-nginx --port=8080
oc status
``` 
### adjust settings for the created route:
Go to:
Application -> Routes -> accstats-nginx -> Security
1. Check "Secured route"
2. TLS Termination = Edge
3. Insecure traffic = redirect

## Verify that proxying works:
open [http://acc-stats.web.cern.ch/acc-stats/](http://acc-stats.web.cern.ch/acc-stats/)

## Remove proxy server:
```
oc delete all --selector app=accstats-nginx
oc status
```
check stats on [openshift web console](https://openshift.cern.ch/console/project/acc-stats/overview)

